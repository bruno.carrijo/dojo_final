Dado('que eu crio um contato e listo esse contato criado') do
    #Listar todos os contatos
    @response = api_tarefas.listar_todos_contatos
    #Criar um contato
    @response = api_tarefas.criar_contato
    #listar contato criado
    @response = api_tarefas.listar_todos_contatos
end

Quando('eu edito esse contato') do
    #Editar contato criado      
    @response = api_tarefas.editar_dados_contato
end

Então('eu posso apagar o contato criado') do
    #Apagar contato criado      
    @response = api_tarefas.deletar_contato    
end