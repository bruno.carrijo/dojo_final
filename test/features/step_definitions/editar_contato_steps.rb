
Dado('que faço um PUT na API para editar um contato') do
    @response = api_tarefas.editar_dados_contato 
 
end

Então('recebo a confirmaçao de sucesso') do
    log @response.to_json
    @code_return = 200
    raise "Erro: o codigo retornado esta errado" if @code_return != @response.code
   
end