Dado('que faço um GET na API') do #para listar um unico contato basta incluir "/9999" no final da URL
 @response = api_tarefas.listar_todos_contatos
end

Então('recebo um resultado') do
    log @response
    raise "Erro: o codigo retornado esta errado" if @response.code != 200
    raise "A API esta retornando vazio" if @response["data"] == nil
end