class ApiDeTarefasStart < SitePrism::Page
  def listar_todos_contatos
    @response = ApiDeTarefas.get("/contacts/#{$RETURNS['contacts']['id']}")
  end
  def criar_contato 
    @body = {
      "name": $name_faker,
      "last_name": $last_name_faker,
      "email": $email_faker,
      "age": $age_faker,
      "phone": $phone_faker,
      "address": $address_faker,
      "state": $state_faker,
      "city": $city_faker
    }.to_json
    @response = ApiDeTarefas.post("/contacts", body: @body)
    puts @response.code
    $RETURNS['contacts']['id'] << @response["data"]["id"]
  end

  def editar_dados_contato 
    @body = {
        "last_name": $last_name_faker,
        "email": $email_faker
    }.to_json
    @response = ApiDeTarefas.put("/contacts/#{$RETURNS['contacts']['id']}", body: @body)
  end

  def deletar_contato 
    @response = ApiDeTarefas.delete("/contacts/#{$RETURNS['contacts']['id']}")
  end
end